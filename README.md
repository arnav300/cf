# Creating a MongoDB and NodeJs K8 Manifests.



## Getting started
##### This Project helps in deploying Mongo Express frontend with MongoDB.

## Running the Project
- Make sure to set secrets in variables in Gitlab CI.
- Clone the project using git clone {URL}
- Run the build.sh script. This will build a local Docker image for Mongo Express(NodeJs app) which will then connect to MongoDB.
- Check the pods and services using "kubectl get all -n mongo-test"
- Navigate to browser and hit localhost:8081 to see Mongo Express page.

NOTE : Please make sure to run "minikube tunnel" so that you can access the app on localhost.

### Pre-requites 

- Make sure minikube is up and running.
- Run to check : minikube dashboard if your minikube cluster is up and running.

